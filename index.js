/*
    @source: https://codeberg.org/jacobwillden/video-filter-file-converter/
    @source: https://github.com/fruiz500/VideoSkip-extension/

    @licstart  The following is the entire license notice this file.

    This file is part of the Video Filter File Converter Project.

    Video Filter File Converter Project Copyright (C) 2023 Jacob Willden
    (Released under the GNU General Public License (GNU GPL) Version 3.0 or later)

    VideoSkip Source Code Copyright (C) 2020, 2021 Francisco Ruiz
    (Released under the GNU General Public License (GNU GPL))

    Some of the code below was derived and modified from the "content2.js"
    source code file in the VideoSkip extension repository (source link
    above), andit is explicitly labled as so. Some of the code below is
    provided by users from StackOverflow, and is explicitly stated as so.
    Such code is released under either the Creative Commons Attribution
    Share-Alike (CC BY-SA) 3.0 or 4.0. I specify Creative Commons as my
    proxy to make the contributions from StackOverflow compatible with
    future versions of the GPL. One line of code below is from Borislav
    Hadzhiev, and is used with permission (explicitly stated as so).

    Afformentioned source code derived and modified by Jacob Willden
    Start Date of Derivation/Modification: June 16, 2023
    Most Recent Date of Derivation/Modification: June 16, 2023

    The Video Filter File Converter Project is free software:
    you can redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version. The project is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    the code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.

    You should have recieved a copy of the GNU General Public License
    along with this project. Otherwise, see: https://www.gnu.org/licenses/

    @licend  The above is the entire license notice for this file.
*/

// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

const hasArrow = new RegExp('^.*-->.*$'); // Derived from mkkabi on StackOverflow (CC BY-SA 4.0): https://stackoverflow.com/questions/31745545/regex-match-line-containing-string

const inputFileElement = document.getElementById('input-file');
const includeCommentsCheckbox = document.getElementById('include-comments');
const convertToEDLButton = document.getElementById('convert-to-edl');

// Convert hour:minute:second string to decimal seconds, function derived and modified from "content2.js" from VideoSkip
function fromHMS(timeString){
    timeString = timeString.replace(/,/, ".");			//in .srt format decimal seconds use a comma
    let time = timeString.split(":");
    if(time.length == 3) {							//has hours
        return parseInt(time[0]) * 3600 + parseInt(time[1]) * 60 + parseFloat(time[2]);
    }
    else if(time.length == 2) {					//minutes and seconds
        return parseInt(time[0]) * 60 + parseFloat(time[1]);
    }
    else {											//only seconds
        return parseFloat(time[0]);
    }
}

function keepLine(line) {
    if(line === '') {
        return false;
    }
    if(line?.match(/^\{/)) {
        return false;
    }
    if(line?.match(/^data/)) {
        return false;
    }
    else {
        return true;
    }
}

function muteOrSkipTag(line) {
    if(line.includes('aud') || line.includes('sou') || line.includes('spe') || line.includes('wor') || line.includes('mut')) { // The keywords are taken from "content2.js" from VideoSkip
        return '1';
    }
    else {
        return '0';
    }
}

function convertTimingLine(line, nextLine) {
    let lineElements = line.split(' ');
    let result = '';
    // console.log('lineElements:', lineElements);
    lineElements.forEach(element => {
        if(!element.match(hasArrow)) {
            result += fromHMS(element) + ' ';
        }
    });
    result += muteOrSkipTag(nextLine);
    return result;
}

function convertCategoryLine(line) {
    return `## ${line}`;
}

// Download data to a file, from aidanjacobson and Kanchu on StackOverflow (CC BY-SA 3.0): https://stackoverflow.com/questions/13405129/create-and-save-a-file-with-javascript
function download(data, name, type) {
    let a = document.createElement("a");
    let file = new Blob([data], {"type": type}),
        url = URL.createObjectURL(file);
    a.href = url;
    a.download = name;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
       document.body.removeChild(a);
       window.URL.revokeObjectURL(url);
    }, 0);
}

function processFile(fileString) {
    let convertedLines = [];
    const lineList = fileString.split(/\r?\n/); // From Borislav Hadzhiev (used with permission): https://bobbyhadz.com/blog/javascript-split-string-by-newline
    const keptLines = lineList.filter(line => keepLine(line));
    for(let i = 0; i < keptLines.length; i++) {
        let line = keptLines[i];
        let nextLine = keptLines[i + 1];
        if(line?.match(hasArrow)) {
            convertedLines = [...convertedLines, convertTimingLine(line, nextLine)];
        }
        else if(includeCommentsCheckbox.checked) {
            convertedLines = [...convertedLines, convertCategoryLine(line)];
        }
    }
    let finalText = '';
    for(let line of convertedLines) {
        finalText += line + '\n';
    }
    download(finalText, prompt('Please enter the file name:') + '.edl', 'text/plain');
    inputFileElement.value = ''; // Clears FileReader buffer, from Mamun on StackOverflow (CC BY-SA 3.0): https://stackoverflow.com/questions/37475067/how-to-clear-files-selection-from-file-input-after-the-data-has-successfully-b?rq=3
}

function previewFile() {
    const [file] = inputFileElement.files;
    const reader = new FileReader();

    reader.addEventListener('load', () => {
        processFile(reader.result)
    }, false);

    if (file) {
      reader.readAsText(file);
    }
}

inputFileElement.addEventListener('change', () => previewFile());

// @license-end