# Video Filter File Converter

[Project Source Code Location](https://codeberg.org/jacobwillden/video-filter-file-converter/)

## General Information

This project is intended to allow easy conversion between different formats for files used to filter objectionable content from videos, based on the user's preferences (as is specifically allowed in the United States under [the Family Movie Act of 2005](https://www.congress.gov/bill/109th-congress/house-bill/357?q=H.R.+357+(109))).

Currently, the only conversion that this project provides is [VideoSkip](https://videoskip.weebly.com/) files to [Edit Decision List (EDL) files that can be used by programs like Kodi](https://kodi.wiki/view/Edit_decision_list), but I intend to add other conversion options in the future.

## Usage Instructions
You can either use [a hosted version of the app on Netlify](https://video-filter-file-converter.netlify.app/), or you can self-host it using the following instructions:

1. Download the repository and extract the ZIP file
2. Open the "index.html" file in a browser (like Chrome, Firefox, Safari, Edge, or Brave)
3. Upload a filter file for conversion (with any specific settings on the page set before uploading)
4. Enter a name for the converted file and you're done!

## Legal
This project is provided under the GNU General Public License (GNU GPL) version 3 or any later version. Some code was derived from the [VideoSkip extension project by Francisco Ruiz](https://github.com/fruiz500/VideoSkip-extension/) and various contributors on [StackOverflow](https://stackoverflow.com/). The contributions from StackOverflow, being posted after 2011 and [according to the site's documentation](https://stackoverflow.com/help/licensing), are released under either [Creative Commons Attribution Share-Alike 3.0](https://creativecommons.org/licenses/by-sa/3.0/) or [4.0](https://creativecommons.org/licenses/by-sa/4.0/). The latter license [is compatible with the GPL version 3 or later, on the condition that I specify Creative Commons as my proxy](https://www.gnu.org/licenses/license-list.html#ccbysa), and the former license [is forwards-compatible, so it can be upgraded to verison 4.0 automatically](https://meta.stackoverflow.com/questions/271293/use-stack-overflow-answer-in-gpl-software-how-to-ask-for-permission). One line of code in the project is from the ["How to Split a String by Newline in JavaScript" tutorial by Borislav Hadzhiev](https://bobbyhadz.com/blog/javascript-split-string-by-newline), and is used with permission. More licensing information can be found in the included source code files.

`SPDX-License-Identifier: GPL-3.0-or-later`

If the file(s) you are converting are not released under an explicit license (like most files on [VideoSkip Exchange](https://videoskip.org/exchange/)), I suggest only converting the file(s) for your personal use (without distributing them). For any files that do have an explicit license, such as the Creative Commons Zero or Creative Commons Attribution License, you are free to both use and distribute under the terms of whichever license they are released under.

This project and its outputs do not alter video files at all, but instead allow "users choose to see or not to see parts of the content" (quoted from the [Read Me file for the VideoSkip extension](https://github.com/fruiz500/VideoSkip-extension/blob/master/README.md)). It also does not enable unauthorized access to video files.

The videos that the outputted files allow filtering of belong to their respective copyright holders. We claim no affliation or endorsement from any of these copyright holders.

Notice to All Users: When watching a motion picture (referring to a movie, television show, etc) using the files converted with this project, the performance of the motion picture is altered from the performance intended by the director or copyright holder of the motion picture.

Todo: Add VideoSkip to MCF support